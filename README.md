Start for development:
```shell
docker-compose -f docker-compose.dev.yml up --build -d
```

Start for production:
```shell
docker-compose up
```