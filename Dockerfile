FROM node:12.11.0 as builder
WORKDIR /app
COPY package*.json ./
RUN npm i
COPY . .
RUN npm run build

FROM node:12.11.0
WORKDIR /app
COPY package*.json ./
RUN npm i --production
COPY --from=builder /app/dist ./dist
EXPOSE 3000
CMD node dist/index.js
