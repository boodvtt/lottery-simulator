declare namespace Express {
  export interface Token {
    user_id: string;
    exp: number; // Expire time
    iat: number; // Issued at
  }

  export interface Request {
    identity: Token;
  }
}
