import { DataTypes } from "sequelize";
import db from "../repo/_base";

export const DrawModel = db.define(
  "draw",
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    start_on: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    end_on: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    win_ticket: {
      type: DataTypes.INTEGER,
    },
  },
  {
    tableName: "draws",
    timestamps: false,
    freezeTableName: true,
  }
);
