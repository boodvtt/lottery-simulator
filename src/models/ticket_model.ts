import { DataTypes } from "sequelize";
import db from "../repo/_base";

export const TicketModel = db.define(
  "ticket",
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    draw_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    tableName: "tickets",
    timestamps: false,
    freezeTableName: true,
  }
);
