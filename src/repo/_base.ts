import sequelize from 'sequelize';

let db = new sequelize.Sequelize({
    host: process.env.POSTGRES_HOST,
    database: process.env.POSTGRES_DB,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    pool: {
        max: 5,
        min: 0,
    },
    dialect: 'postgres',
    timezone: 'utc',
    logging: false,
});

export default db;

