import express from "express";
import { handleGetCurDraw } from "../controllers/draw_controller";

export function applyDrawRoutes(app: express.Express){
    app.get('/draw', handleGetCurDraw);
}
