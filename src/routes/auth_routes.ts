import express from "express";
import { handleUserLogin, handleUserRegister } from "../controllers/auth_controller";

export function applyAuthRoutes(app: express.Express) {
  app.post("/login", handleUserLogin);
  app.post("/register", handleUserRegister);
}
