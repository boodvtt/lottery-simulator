import express from "express";

export function applyStatusRoutes(app: express.Express) {

  app.get("/ping", (req, res, next) => {
    res.json({
      result: "success",
      data: "pong",
    });
  });
}
