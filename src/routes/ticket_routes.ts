import express from "express";
import {
  handleGetTickets,
  handleBuyTicketOnCurDraw,
} from "../controllers/ticket_controller";
import { verifyUserMiddleware } from "../middlewares/jwt";

export function applyTicketRoutes(app: express.Express) {
  app.post("/tickets", verifyUserMiddleware, handleBuyTicketOnCurDraw);
  app.get("/tickets", verifyUserMiddleware, handleGetTickets);
}
