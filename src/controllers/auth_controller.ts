import bcrypt from "bcrypt";
import { RequestHandler } from "express";
import { asyncHandler } from "../common/async_handler";
import { createUserToken } from "../middlewares/jwt";
import { UserModel } from "../models/user_model";

export const handleUserLogin: RequestHandler = asyncHandler(
  async (req, res, next) => {
    const { uid, pw } = req.body;

    if (uid && pw) {
      try {
        const user = await UserModel.findOne({
          attributes: ["id", "pw"],
          where: {
            uid: uid,
          },
        });

        if (user && bcrypt.compareSync(pw, user["pw"])) {
          res.send({
            status: "success",
            token: createUserToken({ user_id: user["id"] }),
          });
          return;
        }
        res.status(403).send({
          status: "failed",
          message: "uid or pw incorrect",
        });
        return;
      } catch (err) {
        res.sendStatus(502);
        return;
      }
    }

    res.sendStatus(403);
  }
);

export const handleUserRegister: RequestHandler = asyncHandler(
  async (req, res, next) => {
    const { uid, pw } = req.body;

    if (uid && pw) {
      try {
        const exists = await UserModel.findOne({
          attributes: ["id"],
          where: {
            uid: uid,
          },
        });

        if (exists === null) {
          const user = UserModel.build({
            uid: uid,
            pw: bcrypt.hashSync(pw, 10),
          });
          await user.save();
          res.status(200).send({
            status: "success",
          });
          return;
        }

        res.status(403).send({
          status: "failed",
          message: "uid exists",
        });
        return;
      } catch (err) {
        console.log(err);
        res.sendStatus(502);
        return;
      }
    }

    res.sendStatus(403);
  }
);
