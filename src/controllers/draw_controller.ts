import { RequestHandler } from "express";
import Sequelize from "sequelize";
import { asyncHandler } from "../common/async_handler";
import { DrawModel } from "../models/draw_model";

export const handleGetCurDraw: RequestHandler = asyncHandler(
  async (req, res, next) => {
    try {
      const draw = await DrawModel.findAll({
        attributes: ["id" ,"start_on", "end_on"],
        where: {
          start_on: {
            [Sequelize.Op.lt]: (new Date()).toUTCString(),
          },
          end_on: {
            [Sequelize.Op.gt]: (new Date()).toUTCString(),
          },
        },
      });

      res.send({
        status: "Success",
        data: draw
      });
    } catch (err) {
      res.send({
        err,
      });
    }
  }
);
