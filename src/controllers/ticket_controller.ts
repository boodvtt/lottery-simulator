import { RequestHandler } from "express";
import { asyncHandler } from "../common/async_handler";
import { DrawModel } from "../models/draw_model";
import { TicketModel } from "../models/ticket_model";

export const handleBuyTicketOnCurDraw: RequestHandler = asyncHandler(
  async (req, res, next) => {
    const { user_id } = req.identity;
    const { draw_id } = req.body;

    if (draw_id !== undefined) {
      const draw = await DrawModel.findOne({
        attributes: ["start_on", "end_on"],
        where: {
          id: draw_id,
        },
      });

      if (draw === null) {
        res.status(403).send({
          status: "failed",
          message: "Draw not found!",
        });
        return;
      } else {
        const start = Date.parse(draw["start_on"]);
        const end = Date.parse(draw["end_on"]);

        if ((start < Date.now() && Date.now() < end) === false) {
          res.status(403).send({
            status: "failed",
            message: "Draw has been ended!",
          });
          return;
        }
      }

      const ticket = await TicketModel.findOne({
        where: {
          draw_id: draw_id,
          user_id: user_id,
        },
      });

      if (ticket !== null) {
        res.status(403).send({
          status: "failed",
          message: "You have take part in this draw already!",
        });
        return;
      } else {
        const newTicket = TicketModel.build({
          draw_id: draw_id,
          user_id: user_id,
        });

        const result = await newTicket.save();

        res.send({
          status: "success",
          data: result,
        });
        return;
      }
    } else {
      res.status(403).send({
        status: "failed",
        message: "invalid input",
      });
      return;
    }
  }
);

export const handleGetTickets: RequestHandler = asyncHandler(
  async (req, res, next) => {
    const { user_id } = req.identity;

    try {
      const tickets = await TicketModel.findAll({
        where: {
          user_id: user_id,
        },
      });

      const draws = await DrawModel.findAll({
        attributes: ["id", "win_ticket"],
        where: {
          id: tickets.map((e) => e["draw_id"]),
        },
      });

      const mapped = tickets.map((t) => {
        const json = t.toJSON();
        const draw = draws.find((d) => d["id"] === t["draw_id"]);
        json["win"] = draw["win_ticket"] === t["id"];
        return json;
      });

      res.send({
        status: "success",
        data: { tickets: mapped },
      });
    } catch (err) {
      res.send({
        err,
      });
    }
  }
);
