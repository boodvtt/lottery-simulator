import express, { json } from "express";
import dotenv from "dotenv";
import { applyStatusRoutes } from "./routes/status_routes";
import { applyDrawRoutes } from "./routes/draw_routes";
import { applyAuthRoutes } from "./routes/auth_routes";
import { applyTicketRoutes } from "./routes/ticket_routes";
import { createDraw, setUpDrawScheduler } from "./services/cron";

dotenv.config();

const app = express();
const port = process.env.APP_PORT;

app.use(json());

setUpDrawScheduler();
createDraw();

applyAuthRoutes(app);
applyDrawRoutes(app);
applyTicketRoutes(app);
applyStatusRoutes(app);

app.use("*", (req, res) => res.sendStatus(404));

app.listen(port, () => {
  console.log(`App listening at port ${port}`);
});
