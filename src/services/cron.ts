import cron from "node-cron";
import Sequelize from "sequelize";
import { DrawModel } from "../models/draw_model";
import { TicketModel } from "../models/ticket_model";

export function setUpDrawScheduler() {
  cron.schedule(process.env.DRAW_PERIODIC_FIELD, async () => {
    await _processLastDraw();
  });
}

async function _processLastDraw() {
  try {
    const draw = await DrawModel.findOne({
      order: [["id", "DESC"]],
      where: {
        win_ticket: null,
      },
    });

    if (draw !== null) {
      const id = draw["id"];

      const tickets = await TicketModel.findAll({
        where: { draw_id: id },
      });

      if (tickets.length <= 1) {
        // update draw frame, add 1 minute
        const newEndTime = new Date();
        newEndTime.setTime(Date.now() + 60 * 1000);
        draw.update({
          end_on: newEndTime.toUTCString(),
        });
      } else {
        const ticket_nos = tickets.map((e) => e["id"]);
        const winner =
          ticket_nos[Math.floor(Math.random() * ticket_nos.length)];
        await draw.update({
          win_ticket: winner,
        });
        await createDraw();
      }
    }
  } catch (err) {
    console.log(err);
  }
}

export async function createDraw() {
  try {
    const cur_draw = await DrawModel.findOne({
      attributes: ["id"],
      where: {
        start_on: {
          [Sequelize.Op.lt]: new Date().toUTCString(),
        },
        end_on: {
          [Sequelize.Op.gt]: new Date().toUTCString(),
        },
      },
    });

    if (cur_draw === null) {
      const now = Date.now();
      const start = now;
      const end = now + 60 * 1000;

      const startDate = new Date();
      startDate.setTime(start);

      const endDate = new Date();
      endDate.setTime(end);

      const draw = DrawModel.build({
        start_on: startDate.toUTCString(),
        end_on: endDate.toUTCString(),
      });

      await draw.save();
    }
  } catch (err) {
    console.log(err);
  }
}
