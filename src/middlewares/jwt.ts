import jwt from "express-jwt";
import { sign } from "jsonwebtoken";

export const verifyUserMiddleware = jwt({
  secret: process.env.JWT_SECRET,
  algorithms: ["HS256"],
  userProperty: 'identity'
});

export function createUserToken(data: object): string {
  return sign(data, process.env.JWT_SECRET, {
    algorithm: "HS256",
  });
}
