
CREATE TABLE draws (
    id          serial          PRIMARY KEY,
    start_on    VARCHAR(30)     NOT NULL,
    end_on      VARCHAR(30)     NOT NULL,
    win_ticket  integer         
);

CREATE TABLE users (
    id     serial          PRIMARY KEY,
    "uid"  VARCHAR(10)     UNIQUE NOT NULL,
    pw     VARCHAR(72)     NOT NULL
);

CREATE TABLE tickets (
    id          serial          PRIMARY KEY,
    draw_id     integer         NOT NULL,
    user_id     integer         NOT NULL
);